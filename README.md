## Ampify Engineering Placement - C++ Task - Leon Paterson-Stephens

To make the calculator more modular I decided to re-work the 'tokeniser' to support 
a more dynamic approach to parsing based on regex expressions.
I have commented through my logic regarding splitting tokens, the method for splitting 
tokens resides in Calculator, Calculator::ReadTokens. Each operator has an associated 
function to it to perform the given operation. Constants are evaluated when reading the 
tokens, a list of regex and values is used when a value can not be parsed using std::stod.

For the calculator itself I have implemented a primitive version of the 
[Shunting Yard Algorithm](https://en.wikipedia.org/wiki/Shunting-yard_algorithm)
to align the operators with postfix notation 
([Reverse Polish Notation](https://en.wikipedia.org/wiki/Reverse_Polish_notation)), 
hence the need for both precedence and associativity in each operator. The final RPN 
expression is then evaluated. I found this resource really helpful,
[Evaluation of RPN using a stack](https://isaaccomputerscience.org/concepts/dsa_toc_rpn).
The calculator now supports more complex expressions taking into account BODMAS. 
Inputs such as, `"5*6*pi"`, `"G*23/4-11"` and `"G/22/22*234+2"` can now be computed.

New constants can be added quickly, the implementation for the constant PI can be seen below.
```cpp
    Calculator::Cnst pi {
        .regex = std::regex("^\\w*pi\\b$", std::regex_constants::icase),
        .val = 3.14159
    }
```

> Currently Supported Constants
* g/G - Acceleration due to gravity. 
* pi/PI - The constant PI.

It is important to be able to run automated tests quickly. As a result of the new method 
for reading tokens, the ResultChecker class needed some tweaking. I have created a new 
Validator structure to mimic the expected [INFIX](https://en.wikipedia.org/wiki/Infix_notation) 
token pattern for a given input. Multiple tests can be ran for a single validator. 
Values are also validated. A test can be run easily through the cli using the command `.test`.

Here is an example validator,
```cpp
    std::vector<ResultChecker::Validator> validator
    {
        {
            .opr = false,
            .val = 6
        },
        {
            .opr = true,
        },
        {
            .opr = false,
            .val = 9
        }
    };

    tokens = calculator.ReadTokens("6*9");
    ResultChecker::ValidateTokens(tokens, validator);

    tokens = calculator.ReadTokens("6 * 9");
    ResultChecker::ValidateTokens(tokens, validator);

    ResultChecker::Check(calculator.Calculate(tokens), 54);
```

I have added a read, eval print loop (REPL). New commands can be added quickly. 
The structure of a 'cmd' can be seen below. Each command has a handler function 
that is invoked when the associated regex is matched from the input. The same 
input is passed to the invoked function. A prompt for user input is also displayed. 
Un-recognised commands will result in an error displayed in the console.

> Currently Supported Commands
* `.help` - Display information about other available commands. 
* `.q` - Exit the application.
* `.test` - Perform tests via defined validators.

```cpp

/**
 * Data structure containing definition for a command.
 *
 * @var Cmd::name
 * Friendly name displayed in CLI;
 *
 * @var Cmd::regex
 * Pattern to match, regex.
 *
 * @var Cmd::help
 * Information about the command, displayed when typing .help
 *
 * @var Cmd::handler
 * Function invoked when regex is matched.
 *
 */
struct Cmd
{
    std::string name;
    std::regex regex;
    std::string help;
    std::function<void(std::string)> handler;
};

```

## CMake + Jetbrains CLion