//
// Focusrite Placement Task - C++
// Created by Leon Paterson-Stephens on 15/02/2021.
//

#include <optional>
#include <string>
#include <iostream>
#include <vector>
#include <regex>

#include "result_checker.h"
#include "calculator.h"
#include "utils/cmd_logger.h"
#include "utils/cmd.h"
#include "utils/cmd_eval.h"


std::vector<Calculator::Cnst> defaultConstants {
    {
        .regex = std::regex("^\\w*pi\\b$", std::regex_constants::icase),
        .val = 3.14159
    },
    {
        .regex = std::regex("^\\w*g\\b$", std::regex_constants::icase),
        .val = 9.80665
    }
};

void test ()
{
    std::vector<Calculator::Cnst> constants = defaultConstants;

    Calculator calculator(constants);
    std::vector<Calculator::Token> tokens;

    {
        std::vector<ResultChecker::Validator> validator
        {
            {
                .opr = false,
                .val = 6
            },
            {
                .opr = true,
            },
            {
                .opr = false,
                .val = 9
            }
        };

        tokens = calculator.ReadTokens("6*9");
        ResultChecker::ValidateTokens(tokens, validator);

        tokens = calculator.ReadTokens("6 * 9");
        ResultChecker::ValidateTokens(tokens, validator);

        ResultChecker::Check(calculator.Calculate(tokens), 54);
    }

    {
        std::vector<ResultChecker::Validator> validator
        {
            {
                .opr = false,
                .val = 25
            },
            {
                .opr = true,
            },
            {
                .opr = false,
                .val = 4
            },
            {
                .opr = true,
            },
            {
                .opr = false,
                .val = 2
            }
        };

        tokens = calculator.ReadTokens("25 * 4 * 2");
        ResultChecker::ValidateTokens(tokens, validator);

        ResultChecker::Check(calculator.Calculate(tokens), 200);
    }

    {
        std::vector<ResultChecker::Validator> validator
        {
            {
                .opr = false,
                .val = 3.14159
            }
            ,
            {
                .opr = true,
            },
            {
                .opr = false,
                .val = 2
            }
        };

        tokens = calculator.ReadTokens("PI *       2");
        ResultChecker::ValidateTokens(tokens, validator);

        ResultChecker::Check(calculator.Calculate(tokens), 6.282);
    }

    CmdLogger::Log(CmdLogger::LogLevel::debug, "All tests passed validation with zero errors!");
}

void run ()
{
    bool processExiting = false;

    // Thankyou: https://patorjk.com/software/taag/ -> Text to ASCII
    std::string asciIntro = R"(
  ______                    _____      _
 |  ____|                  / ____|    | |
 | |__ ___   ___ _   _ ___| |     __ _| | ___
 |  __/ _ \ / __| | | / __| |    / _` | |/ __|
 | | | (_) | (__| |_| \__ \ |___| (_| | | (__
 |_|  \___/ \___|\__,_|___/\_____\__,_|_|\___|
)";

    CmdLogger::Write(STR_FOCUS, asciIntro, true, true);

    std::vector<Cmd> cmdRegister;

    cmdRegister.push_back(Cmd {
        .name = "Help",
        .regex = std::regex("^\\w*.help\\b$", std::regex_constants::icase),
        .help = "Type \'.help\' to get information about all available commands!!",
        .handler = [&cmdRegister](std::string input){
            for(size_t i = 0; i < cmdRegister.size(); i++) {
                std::stringstream stringStream;
                stringStream << cmdRegister[i].name << ": " << cmdRegister[i].help;

                CmdLogger::Write(STR_PROMPT, stringStream.str(), true, true);
            }
        }
    });

    cmdRegister.push_back(Cmd {
        .name = "Quit",
        .regex = std::regex("^\\w*.quit\\b$|^\\w*.q\\b$", std::regex_constants::icase),
        .help = "Type \'.quit\' / \'.q\' to exit the program.",
        .handler = [&processExiting](std::string input){
            processExiting = true;
        }
    });

    cmdRegister.push_back(Cmd {
        .name = "Test",
        .regex = std::regex("^\\w*.test\\b$", std::regex_constants::icase),
        .help = "Type \'.test\' to run automated tests on the calculator!",
        .handler = [](std::string input){
            test();
        }
    });

    std::vector<Calculator::Cnst> constants = defaultConstants;

    Calculator calculator(constants);

    cmdRegister.push_back(Cmd {
            .name = "Calculate",
            .regex = std::regex("^[^.\\s].*"),
            .help = "Perform calculation.",
            .handler = [&calculator](std::string input){
                std::vector<Calculator::Token> tokens;

                try {
                    tokens = calculator.ReadTokens(input);
                } catch(std::runtime_error err)
                {
                    CmdLogger::Log(CmdLogger::LogLevel::error, "Error parsing tokens! Please check your input string.");
                    return;
                }

                double result = calculator.Calculate(tokens);
                CmdLogger::Write(STR_PROMPT, std::to_string(result), true, true);
            }
    });

    CmdLogger::Write(STR_DEBUG, "Calculator running, type .help to get information about all available commands!!", true, true);

    while(!processExiting) {
        CmdLogger::Prompt(">");

        std::string input;
        std::getline(std::cin, input);

        CmdEval::EvaluateInput(input, cmdRegister);
    }

    CmdLogger::Write(STR_WARN, "Calculator closing, thank you.", true, true);
}

int main (int argc, const char * argv[])
{
    if (argc > 1 && std::string (argv[1]) == "--test")
        test ();
    else
        run ();

    return 0;
}
