//
// Focusrite Placement Task - C++
// Created by Leon Paterson-Stephens on 15/02/2021.
//

#ifndef CPP_TASK_CALCULATOR_H
#define CPP_TASK_CALCULATOR_H

#include <map>
#include <functional>
#include <string>
#include <vector>
#include <regex>
#include <cmath>

#include "utils/cmd_logger.h"

class Calculator
{
public:

    /**
     * Associativity of an operator.
     */
    enum class Associativity {
        LEFT,
        RIGHT
    };

    /**
     * @var Opr::regex
     * Regex expression associated with the operator.
     * @var Opr::func
     * Evaluation function for the given operator.
     * @var Opr::prec
     * Precedence for the operator, used in the shunting yard algorithm.
     * @var Opr::associativity
     * Associativity for the operator, used in the shunting yard algorithm.
     */
    struct Opr
    {
        std::regex regex;
        std::function<double(double, double)> func;
        size_t prec;
        Associativity associativity;
    };

    /**
     * @var Cnst::regex
     * Regex expression associated with the constant.
     * @var Cnst::val
     * The value of the constant.
     */
    struct Cnst
    {
        std::regex regex;
        double val;
    };

    struct Token
    {
        Opr* opr;
        double val;
    };

    /**
     * @param constants
     * Constants supported by calculator instance.
     */
    Calculator(
            std::vector<Cnst> constants)
            :
            _constants(constants)
            {}

    /**
     * Perform calculation on std::vector<Token> given INFIX notation.
     * Will first convert input INFIX to RPN using the Shunting yard Algorithm then evaluate the expression as a stack.
     *
     * Both very useful resources...
     * @cite https://en.wikipedia.org/wiki/Shunting-yard_algorithm
     * @cite https://isaaccomputerscience.org/concepts/dsa_toc_rpn
     *
     * @param tokens
     * @return
     */
    double Calculate(std::vector<Token> tokens) const;

    /**
     *
     * @param input
     * @return std::vector<Token>
     * List of Tokens from the given input.
     * Known constants are initialized in the constructor.
     */
    std::vector<Token> ReadTokens(std::string input);

private:
    std::vector<Cnst> _constants;
    std::vector<Opr> _operators {
        {
                .regex = std::regex("[+]"),
                .func = [](double a, double b) -> double {
                    return a + b;
                },
                .prec = 1,
                .associativity = Calculator::Associativity::LEFT
        },
        {
                .regex = std::regex("[-]"),
                .func = [](double a, double b) -> double {
                    return a - b;
                },
                .prec = 1,
                .associativity = Calculator::Associativity::LEFT
        },
        {
                .regex = std::regex("[*]"),
                .func = [](double a, double b) -> double {
                    return a * b;
                },
                .prec = 2,
                .associativity = Calculator::Associativity::LEFT
        },
        {
                .regex = std::regex("[/]"),
                .func = [](double a, double b) -> double {
                    return a / b;
                },
                .prec = 2,
                .associativity = Calculator::Associativity::LEFT
        },
        {
                .regex = std::regex("[\\^]"),
                .func = [](double a, double b) -> double {
                    return std::pow(a, b);
                },
                .prec = 3,
                .associativity = Calculator::Associativity::RIGHT
        }
    };

    /**
     * @deprecated Method has been deprecated as it doesn't support parsing of multi-char tokens. This would not allow regex expressions to contain operators such as 'sin'.
     * @param token
     * @return
     */
    Opr* getOperator(std::string token);

    /**
     * @param token
     * Input token to parse, should either be a constant known to the calculator instance or a double.
     * @return
     * @throws Runtime exception if the token string either isn't a known constant or cant be parsed using std::stod.
     *
     * @bug std::stod seems to interpret "5asdasd" as 5.00, not a huge issue but possibly something to look at.
     */
    double getVal(std::string token);
};


#endif //CPP_TASK_CALCULATOR_H
