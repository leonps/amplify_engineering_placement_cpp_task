//
// Focusrite Placement Task - C++
// Created by Leon Paterson-Stephens on 15/02/2021.
//

#include "result_checker.h"

void ResultChecker::Check(double value, double expected, double range)
{
    return assert(std::abs (int(value - expected)) <= range);
}

void ResultChecker::ValidateTokens(std::vector<Calculator::Token> tokens, std::vector<Validator> validators){

    // Initially check that the size of the validator[] matches that of the tokens.
    assert(tokens.size() == validators.size());

    for(size_t i = 0; i < tokens.size(); i++)
    {
        if(validators[i].opr)
        {
            // If the validators[i] is expected to be an operator, assert this assumption checking the tokens[i] is not a nullptr.
            assert(tokens[i].opr != nullptr);
        } else {
            // If the validators[i] is a value, assert that value matches the tokens[i].val.
            assert(tokens[i].val == validators[i].val);
        }
    }
}
