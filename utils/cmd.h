//
// Focusrite Placement Task - C++
// Created by Leon Paterson-Stephens on 15/02/2021.
//

#ifndef CPP_TASK_CMD_H
#define CPP_TASK_CMD_H

#include <string>
#include <functional>
#include <regex>

/**
 * Data structure containing definition for a command.
 *
 * @var Cmd::name
 * Friendly name displayed in CLI;
 *
 * @var Cmd::regex
 * Pattern to match, regex.
 *
 * @var Cmd::help
 * Information about the command, displayed when typing .help
 *
 * @var Cmd::handler
 * Function invoked when regex is matched.
 *
 */
struct Cmd
{
    std::string name;
    std::regex regex;
    std::string help;
    std::function<void(std::string)> handler;
};

#endif //CPP_TASK_CMD_H
