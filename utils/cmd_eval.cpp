//
// Focusrite Placement Task - C++
// Created by Leon Paterson-Stephens on 15/02/2021.
//

#include "cmd_eval.h"

void CmdEval::EvaluateInput(std::string input, std::vector<Cmd> cmdRegister) {
    for(size_t i = 0; i < cmdRegister.size(); i++)
    {
        if(std::regex_match(input, cmdRegister[i].regex))
        {
            cmdRegister[i].handler(input);
            return;
        }
    }

    CmdLogger::Log(CmdLogger::LogLevel::error, "Unkonwn command. Type .help to get information about all available commands!!");
}
