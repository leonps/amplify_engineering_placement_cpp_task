//
// Focusrite Placement Task - C++
// Created by Leon Paterson-Stephens on 15/02/2021.
//

#include "cmd_logger.h"

void CmdLogger::Log(LogLevel logLevel, std::string message) {
    std::stringstream stringStream;

    switch(logLevel)
    {
        case LogLevel::debug:
            stringStream << "Debug: " << message;
            std::cout << ansiFormatter(STR_DEBUG, stringStream.str(), true, true).rdbuf();
            break;
        case LogLevel::error:
            stringStream << "Error: " << message;
            std::cout << ansiFormatter(STR_ERROR, stringStream.str(), true, true).rdbuf();
            break;
        case LogLevel::warn:
            stringStream << "Warning: " << message;
            std::cout << ansiFormatter(STR_WARN, stringStream.str(), true, true).rdbuf();
            break;
    }
}

void CmdLogger::Write(std::string escapeSequence, std::string message, bool envelop, bool newLine) {
    std::cout << ansiFormatter(escapeSequence, message, envelop, newLine).rdbuf();
}

void CmdLogger::Prompt(std::string cursor) {
    std::stringstream stringStream;
    stringStream << cursor << " ";
    std::cout << ansiFormatter(STR_PROMPT, stringStream.str(), false, false).rdbuf();
}

std::stringstream CmdLogger::ansiFormatter(std::string escapeSequence, std::string message, bool envelop, bool newLine) {
    std::stringstream  stringStream;

    stringStream << escapeSequence << message;

    if(envelop) {
        stringStream << escapeSequence;
    }

    if(newLine) {
        stringStream << "\n";
    }

    return stringStream;
}