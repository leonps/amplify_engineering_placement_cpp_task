//
// Focusrite Placement Task - C++
// Created by Leon Paterson-Stephens on 15/02/2021.
//

#ifndef CPP_TASK_CMD_EVAL_H
#define CPP_TASK_CMD_EVAL_H

#include <regex>
#include <vector>

#include "cmd.h"
#include "cmd_logger.h"

class CmdEval {
public:
    /**
     * Invokes handler function of Cmd when the FIRST command matching the input is found.
     * In the event no command is found, an error will be logged to the console.
     *
     * @param input
     * @param cmdRegister
     */
    static void EvaluateInput(std::string input, std::vector<Cmd> cmdRegister);
};


#endif //CPP_TASK_CMD_EVAL_H
