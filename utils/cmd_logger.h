//
// Focusrite Placement Task - C++
// Created by Leon Paterson-Stephens on 15/02/2021.
//

#ifndef CPP_TASK_CMD_LOGGER_H
#define CPP_TASK_CMD_LOGGER_H

#include <string>
#include <iostream>
#include <sstream>

/**
 * Global colour definitions, ANSI Escape Codes
 */
#define STR_WARN "\x1b[1;33m"
#define STR_DEBUG "\x1b[1;32m"
#define STR_ERROR "\x1b[1;31m"
#define STR_PROMPT "\x1b[1;36m"

// RED -> Focusrite -> Focus
#define STR_FOCUS "\x1b[1;31m"

class CmdLogger {
public:

    /**
     * LogLevels available.
     */
    enum class LogLevel {
        debug,
        warn,
        error
    };

    /**
     * Display a formatted message in the CLI
     *
     * @param escapeSequence
     * Any valid ANSI Escape Code to modify the appearance of the printed message.
     * @param message
     * Message to be displayed in the console.
     * @param envelop
     * Should the message be surrounded by the escape sequence, removing formatting to trailing text.
     * @param newLine
     * Should the message end with \n
     */
    static void Write(std::string escapeSequence, std::string message, bool envelop, bool newLine);

    /**
     * Simple helper function to display a CLI prompt given a cursor. Will colour the trailing text.
     * @param cursor
     */
    static void Prompt(std::string cursor);

    /**
     * Log a message to the console, not currently used in the typical sense of a 'logger' though it has been handy while testing.
     *
     * todo: Implement file logger
     */
    static void Log(LogLevel logLevel, std::string message);
private:
    static std::stringstream ansiFormatter(std::string escapeSequence, std::string message, bool envelop, bool newLine);
};


#endif //CPP_TASK_CMD_LOGGER_H
