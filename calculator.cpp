//
// Focusrite Placement Task - C++
// Created by Leon Paterson-Stephens on 15/02/2021.
//

#include "calculator.h"

/**
 * DEPRECATED
 */
Calculator::Opr* Calculator::getOperator(std::string token)
{
    for(size_t i = 0; i < _operators.size(); i++)
    {
        if(std::regex_match(token, _operators[i].regex))
        {
            return & _operators[i];
        }
    }

    return nullptr;
}

double Calculator::getVal(std::string token) {

    double val;

    try {
        // Try converting the string to a double
        val = std::stod(token);
        return val;
    }
    catch (const std::invalid_argument & err){
        // Catch errors from std::stod, check if the string is a known constant of the calculator using the constants regex terms.
        for(size_t i = 0; i < _constants.size(); i++)
        {
            if(std::regex_match(token, _constants[i].regex))
            {
                return _constants[i].val;
            }
        }
    }

    // The input string cant be directly converted to double and isn't a known constant of the calculator.
    throw std::runtime_error("Error converting token, no viable conversion found!");
}

std::vector<Calculator::Token> Calculator::ReadTokens(std::string input)
{
    std::vector<Calculator::Token> tokens;

    // Erase whitespace...
    input.erase(std::remove_if(input.begin(), input.end(), isspace), input.end());

    // While tokens remain in the input
    while(input.length() > 0) {
        // Index of best batching operator
        int opr;
        // Best match info
        std::smatch bestMatch;

        for (size_t i = 0; i < _operators.size(); i++) {
            std::smatch m;

            // Check current input string for all operators based on regex term
            if (regex_search(input, m, _operators[i].regex)) {
                int splitOn = m.position();

                if (bestMatch.empty() || splitOn < bestMatch.position()) {
                    // If the current bestMatch is empty || the split position of the match is smaller than the previous best match, then update the current best match...
                    // Possibly quite convoluted...
                    bestMatch = m;
                    opr = i;
                }
            }
        }

        if (!bestMatch.empty()) {
            int splitOn = bestMatch.position();

            // If an operator was found, get the preceding string, push the token if it isn't empty.
            std::string prep = input.substr(0, splitOn);

            if(!prep.empty())
            {
                tokens.push_back(Token { .val = getVal(prep)});
            }

            // Push the 'bestMatch' operator token.
            tokens.push_back(Token{.opr = & _operators[opr]});

            // Cull the string, removing all processed tokens...
            input = input.substr(splitOn + bestMatch.length());
        } else
        {
            // No more operators in the input, getValue of the remaining string.
            tokens.push_back(Token { .val = getVal(input)});
            input.clear();
        }
    }

    return tokens;
}

double Calculator::Calculate(std::vector<Calculator::Token> tokens) const
{
    std::vector<Token> rpn;
    std::vector<Token> stack;

    // Shunting-yard algorithm -> will arrange to RPN, currently wont work for more complex operators... e.g. ()
    // This method is outlined further in the cite for Calculator::Calculate.
    for(size_t i = 0; i < tokens.size(); i++)
    {
        if(tokens[i].opr != nullptr)
        {
            // If the token is an operator
            const Token tokOpr = tokens[i];

            while(!stack.empty())
            {
                // Previous operator
                const Token tokOprBack = stack.back();

                ssize_t prec = tokOpr.opr->prec;
                ssize_t precBack = tokOprBack.opr->prec;

                bool rightAssociative = (tokOpr.opr->associativity == Associativity::RIGHT);

                if((!rightAssociative && prec <= precBack) || (rightAssociative && prec < precBack))
                {
                    // If the current operator is not rightAssociative and is less than or equal to the previous operator.
                    // If the current operator is rightAssociative and has a lower precedence than the previous operator.

                    // Pop it off the stack -> push on to rpn
                    stack.pop_back();
                    rpn.push_back(tokOprBack);

                    continue;
                }

                break;
            }

            stack.push_back(tokOpr);
        } else
        {
            // If the token is a value, push it to rpn
            rpn.push_back(tokens[i]);
        }
    }

    std::reverse(stack.begin(), stack.end());

    // Insert remaining operators on the stack -> rpn
    rpn.insert(rpn.end(), stack.begin(), stack.end());

    std::vector<double> out;

    // Evaluate the rpn token list
    for(size_t i = 0; i < rpn.size(); i++) {
        Token token = rpn[i];

        if(token.opr != nullptr)
        {
            // If the token[i] is an operator pop the last 2 elements and apply the operators function. Push the result back onto the out stack.

            double rhs = out.back();
            out.pop_back();

            double lhs = out.back();
            out.pop_back();

            out.push_back(token.opr->func(lhs, rhs));
        } else {
            // If the token[i] is a value, push the value to the stack.
            out.push_back(token.val);
        }
    }

    return out.back();
}