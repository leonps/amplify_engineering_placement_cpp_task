//
// Focusrite Placement Task - C++
// Created by Leon Paterson-Stephens on 15/02/2021.
//

#ifndef CPP_TASK_RESULT_CHECKER_H
#define CPP_TASK_RESULT_CHECKER_H

#include <cstdlib>
#include <vector>

#include "./calculator.h"

/**
 *
 */
class ResultChecker
{
public:
    struct Validator {
        // Indicates if the validator is an operator.
        bool opr;

        // Value if not an operator.
        double val;
    };

    /**
     *
     * @param value
     * @param expected
     * @param range
     */
    static void Check(double value, double expected, double range = 1e-3);

    /**
     * Evaluate a list of Calculator::Token against an expected set of validators.
     * Value tokens will assert equality to linked validator value.
     * Operator tokens will check for nullptr
     *
     * todo: Potentially add parameter to validator to check the regex of an operator to confirm it is expected.
     *
     * @throws Assertion failed if any validator condition is not met.
     *
     * @param tokens
     * @param validators
     */
    static void ValidateTokens(std::vector<Calculator::Token> tokens, std::vector<Validator> validators);
};

#endif //CPP_TASK_RESULT_CHECKER_H
